#!/usr/bin/python
#server-room-monitoring.py

#Exosite Cloud-enabled DS18b20 Temperature sensors
#Example Exosite CIKS

#Imports
import time
from decimal import *
import urllib
import httplib
import os
import glob
import os.path
import sys
import RPi.GPIO as io
io.setmode(io.BCM)

class Logger(object):
    def __init__(self, filename="Default.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

sys.stdout = Logger("report_temp.log")

#System Setup
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

#w1 DS18b20 Configuration Setting
base_dir = '/sys/bus/w1/devices/'
device_folder = ''
device_file = ''
temp_pin1 = 4

#Exosite portal configuration
cik = 'dbf551a3b9332f1888e7c706bbde9689f33e6c38'
server = 'm2.exosite.com'

#Fluid Detector Configuration Settings

probe_pin1 = 17
probe_pin2 = 25
probe_LED1_pin = 22
probe_LED2_pin = 23

io.setup(probe_pin1, io.IN)
io.setup(probe_pin2, io.IN)
io.setup(probe_LED1_pin, io.OUT)
io.setup(probe_LED2_pin, io.OUT)

def read_temps_multi():
        count = 0
        return_param=''
        return_array={}
        for device in glob.glob(base_dir + '28*'):
                count +=1
                global device_folder
                global device_file
                device_folder = device
                device_file = device_folder + '/w1_slave'
                return_array[sens_name()] = str(read_temp())
        return_param = urllib.urlencode(return_array)
        return return_param

def read_temp_raw():
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    if os.path.isfile(device_file):
        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
    return lines

def sens_name():
    sensor = device_folder[-15:]
    return sensor

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

def read_fluid_sensors():
        count = 0
	probe1_count = 0
	probe2_count = 0
	probe1_alarm = 0
	probe2_alarm = 0
	probe1_triggered = 0
	probe2_triggered = 0
        return_param=''
        return_array={}
        while count <=10:
#		print "Current Count: "+str(count)
                count +=1
                probe1 = io.input(probe_pin1)
                probe2 = io.input(probe_pin2)
                if probe1:
                        probe1_alarm +=1
                        probe1_count +=1
                else:
                        if probe1_alarm >=1:
                                probe1_alarm -= 1
                if probe2:
                         probe2_alarm +=1
                         probe2_count +=1
                else:
                        if probe2_alarm >=1:
                                probe2_alarm -= 1
                if probe1_alarm > 10:
                        probe1_triggered =1
                if probe2_alarm > 10:
                        probe2_triggered =1
                time.sleep(1)
	io.output(probe_LED1_pin, probe1_triggered)
	io.output(probe_LED2_pin, probe2_triggered)
        return_array["Fluid-Probe-1"] = str(probe1_triggered)
        return_array["Fluid-Probe-2"] = str(probe2_triggered)
        return_param = urllib.urlencode(return_array)
	return return_param


count = 0
probe1_count = 0
probe2_count = 0
probe1_alarm = 0
probe2_alarm = 0
probe1_triggered = 0
probe2_triggered = 0

while True:
        count +=1
        probe1 = io.input(probe_pin1)
        probe2 = io.input(probe_pin2)
        if probe1:
        	probe1_alarm +=1
                probe1_count +=1
        else:
                if probe1_alarm >=1:
                	probe1_alarm -= 1
        if probe2:
                probe2_alarm +=1
                probe2_count +=1
        else:
                if probe2_alarm >=1:
                        probe2_alarm -= 1
        if probe1_alarm > 10:
                probe1_triggered =1
        if probe2_alarm > 10:
                probe2_triggered =1
        time.sleep(1)
        io.output(probe_LED1_pin, probe1_triggered)
        io.output(probe_LED2_pin, probe2_triggered)

	print "Count - Probe 1:  "+ str(probe1_count)
	print "Count - Probe 2:  "+ str(probe2_count)
	print "Alarm - Probe 1:  "+ str(probe1_alarm)
	print "Alarm - Probe 2:  "+ str(probe2_alarm)
	print "Trigg - Probe 1:  "+ str(probe1_triggered)
	print "Trigg - Probe 2:  "+ str(probe2_triggered)
	print ""
