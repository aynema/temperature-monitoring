#!/usr/bin/python
#fluid.py
#
#Author: Alex Ynema
#
#Usage fluid.py ID
# where ID= Fluid Probe ID

#Imports
import time
from decimal import *
import os
import os.path
import sys
import RPi.GPIO as io
io.setmode(io.BCM)
from sys import argv as s

#Array of Probe GPIO PIN numbers for each Fluid Probe
probe_pin_list=[17] # Comma Separated Array of Probe GPIO Pins - Can be Empty
#Array of Probe GPIO PIN numbers for each LED
probe_LED_list=[]  # Comma Separated Array of LED GPIO Pins - Can be Empty

def read_fluid_sensor(probe_pin):
	io.setup(probe_pin,io.IN)
	count = 0
        probe_count = 0
        probe_alarm = 0
        probe_triggered = 0
        while count <=3:
                count +=1
                probe = io.input(probe_pin)
                if probe:
                        probe_alarm +=1
                time.sleep(1)
	if probe_alarm >=3:
		return 1
	else:
	        return 0

while len(s) != 1:
	if (int(s[1])<len(probe_pin_list)) or (s[1]<0):
		probe_id=int(s[1])
        	params = read_fluid_sensor(probe_pin_list[probe_id])
		if (int(s[1])<len(probe_LED_list)):
			io.setup(probe_LED_list[int(s[1])],params)
        	print params
        	break
   	else:
        	print 'Probe Number Invalid'
        	break	
