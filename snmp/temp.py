#!/usr/bin/python
#temp.py
#
#Author: Alex Ynema
#
#Usage temp.py ID
# where ID= DS18B20 Probe ID

#Imports
import os
import os.path
from sys import argv as s

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

#w1 DS18b20 Configuration Setting
base_dir = '/sys/bus/w1/devices/'
device_folder = ''
device_file = ''

def read_temp_raw():
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    if os.path.isfile(device_file):
        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
    return lines

def sens_name():
    sensor = device_folder[-15:]
    return sensor

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

while len(s) != 1:
   if os.path.isfile("/sys/bus/w1/devices/"+s[1]+"/w1_slave"):
	device_file="/sys/bus/w1/devices/"+s[1]+"/w1_slave"
	params = read_temp()
   	print params
	break
   else:
	print 'Probe ID Invalid'
	break
if len(s) == 1:
	print 'No Probe Configured'
