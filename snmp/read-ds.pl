#!/usr/bin/perl

$sensor_temp = "";
$attempts = 0;
$temperature = 0;

while ($sensor_temp !~ /YES/g && $attempts < 5)
{
        $sensor_temp = `cat /sys/bus/w1/devices/28-000004a62252/w1_slave 2>&1`;
        if ($sensor_temp =~ /No such file or directory/)
        {
                last;
        }
        elsif ($sensor_temp !~ /NO/g)
        {
        	$sensor_temp =~ /t=(\d+)/i;
		$temperature = ($1)/1000;
	}
        $attempts++;
}
print "$temperature\n";
