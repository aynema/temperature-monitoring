import json
import requests
#import time
from decimal import *
#import urllib
#import httplib
import os
import glob
import os.path
import sys

class Logger(object):
    def __init__(self, filename = "logfile.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def __getattr__(self, attr):
        return getattr(self.terminal, attr)

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass

sys.stdout = Logger("report_temp.log")

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

#w1 DS18b20 Configuration Setting
base_dir = '/sys/bus/w1/devices/'
device_folder = ''
device_file = ''

# Define unchanging URL and Headers
url = 'https://z2u921zkzf2800000.m2.exosite.io/onep:v1/stack/alias'
headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
    'X-Exosite-CIK': 'd0r4hv06npPcRNk4szuXqAdaEf5Izc3CRgzum69y'
}

def read_temps_multi():
        count = 0
        return_param=''
        return_array={}
        for device in glob.glob(base_dir + '28*'):
                count=count+1
                global device_folder
                global device_file
                device_folder = device
                device_file = device_folder + '/w1_slave'
                return_array[sens_name()] = str(read_temp())
        return_param=json.dumps(return_array)
        return return_param

def read_temp_raw():
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def sens_name():
    sensor = device_folder[-15:]
    return sensor

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

# Temp Probes
# Device: #1 - BBIGA-TEMP01
# 28-000005226f32 - Temp - BBIGA-TEMP01 - Module		
#	28-80000026f169		Freezers-Bay07 - 02A07
#	28-80000026db11		Freezers-Bay08 - 02A08
#	28-0000052f5792		Freezers-Bay09 - 02A09
#	28-0000052fac78		Freezers-Bay10 - 02A10
#	28-000006dd28dd		Milk Fridge #1
#   28-80000026f4d6     Egg and Juice Fridge
#

# Build config
config_io = {
    'channels': {
        '28-000005226f32': {
            'display_name': 'BBIGA-TEMP01 - Module',
            'description': 'BBIGA-TEMP01 - Module',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': 1,
                'max': 40
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
    '28-80000026f4d6': {
            'display_name': 'Egg and Juice Fridge',
            'description': 'Egg and Juice Fridge',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': 1,
                'max': 5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-80000026f169': {
            'display_name': 'Freezers-Bay07 - 02A07',
            'description': 'Freezers-Bay07 - 02A07',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-000006dd28dd': {
            'display_name': 'Milk Fridge #1',
            'description': 'Milk Fridge #1',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': 1,
                'max': 5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-0000052fac78': {
            'display_name': 'Freezers-Bay10 - 02A10',
            'description': 'Freezers-Bay10 - 02A10',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-0000052f5792': {
            'display_name': 'Freezers-Bay09 - 02A09',
            'description': 'Freezers-Bay09 - 02A09',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-80000026db11': {
            'display_name': 'Freezers-Bay08 - 02A08',
            'description': 'Freezers-Bay08 - 02A08',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        }
    }
}
config_payload= 'config_io=' + json.dumps(config_io)

# Write config
config_resp = requests.post(url, headers=headers, data=config_payload)

# Build data payload
data_in = read_temps_multi()

# data_payload = 'data_in=' + json.dumps(config_io)
data_payload = 'data_in=' + data_in

# Write data
data_resp = requests.post(url, headers=headers, data=data_payload)
