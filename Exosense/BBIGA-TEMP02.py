import json
import requests
import time
from decimal import *
#import urllib
#import httplib
import os
import glob
import os.path
import sys

class Logger(object):
    def __init__(self, filename = "logfile.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def __getattr__(self, attr):
        return getattr(self.terminal, attr)

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass

sys.stdout = Logger("report_temp.log")

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

#w1 DS18b20 Configuration Setting
base_dir = '/sys/bus/w1/devices/'
device_folder = ''
device_file = ''

# Define unchanging URL and Headers
url = 'https://z2u921zkzf2800000.m2.exosite.io/onep:v1/stack/alias'
headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
#    'X-Exosite-CIK': 'd0r4hv06npPcRNk4szuXqAdaEf5Izc3CRgzum69y'
    'X-Exosite-CIK': 'e2a14d7dcd2fa515d68e454c1deb4d4745c5e604'
}

def read_temps_multi():
        count = 0
        return_param=''
        return_array={}
        for device in glob.glob(base_dir + '28*'):
                count=count+1
                global device_folder
                global device_file
                device_folder = device
                device_file = device_folder + '/w1_slave'
                return_array[sens_name()] = str(read_temp())
        return_param=json.dumps(return_array)
        return return_param

def read_temp_raw():
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def sens_name():
    sensor = device_folder[-15:]
    return sensor

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

# Temp Probes
# Device: #1 - BBIGA-TEMP02
# 28-000005226f32 - Temp - BBIGA-TEMP02 - Module	
#
#	28-80000026c703		Freezers-Bay01 - 02A01
#	28-80000026ed76		Freezers-Bay02 - 02A02
#	28-00000674854e		Freezers-Bay03 - 02A03
#	28-0000067447a9		Freezers-Bay04 - 02A04
#	28-000006742b16		Freezers-Bay05 - 02A05
#	28-000006747957		Freezers-Bay06 - 02A06
#

# Build config
config_io = {
    'channels': {
#        '28-000005226f32': {
#            'display_name': 'BBIGA-TEMP02 - Module',
#            'description': 'BBIGA-TEMP02 - Module',
#            'properties': {
#                'data_type': 'TEMPERATURE',
#                'data_unit': 'DEG_CELSIUS',
#                'precision': 2,
#                'min': 0,
#                'max': 40
#            },
#            'protocol_config': {
#                'report_rate': 60000,
#		'timeout': 120000
#            }
 #       },
	'28-000006747957': {
            'display_name': 'Freezers-Bay06 - 02A06',
            'description': 'Freezers-Bay06 - 02A06',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-000006742b16': {
            'display_name': 'Freezers-Bay05 - 02A05',
            'description': 'Freezers-Bay05 - 02A05',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-0000067447a9': {
            'display_name': 'Freezers-Bay04 - 02A04',
            'description': 'Freezers-Bay04 - 02A04',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
	'28-00000674854e': {
            'display_name': 'Freezers-Bay03 - 02A03',
            'description': 'Freezers-Bay03 - 02A03',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
    '28-80000026c703': {
            'display_name': 'Freezers-Bay01 - 02A01',
            'description': 'Freezers-Bay01 - 02A01',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        },
    '28-80000026ed76': {
            'display_name': 'Freezers-Bay02 - 02A02',
            'description': 'Freezers-Bay02 - 02A02',
            'properties': {
                'data_type': 'TEMPERATURE',
                'data_unit': 'DEG_CELSIUS',
                'precision': 2,
                'min': -50,
                'max': -5
            },
            'protocol_config': {
                'report_rate': 60000,
		'timeout': 120000
            }
        }
    }
}
config_payload= 'config_io=' + json.dumps(config_io)

# Write config
config_resp = requests.post(url, headers=headers, data=config_payload)

# Build data payload
data_in = read_temps_multi()

# data_payload = 'data_in=' + json.dumps(config_io)
data_payload = 'data_in=' + data_in

# Write data
data_resp = requests.post(url, headers=headers, data=data_payload)
