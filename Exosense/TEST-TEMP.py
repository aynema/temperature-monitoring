import json
import requests
#import time
from decimal import *
#import urllib
#import httplib
import os
import glob
import os.path
import sys

class Logger(object):
    def __init__(self, filename="Default.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

sys.stdout = Logger("report_temp.log")

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

#w1 DS18b20 Configuration Setting
base_dir = '/sys/bus/w1/devices/'
device_folder = ''
device_file = ''

def read_temps_multi():
        count = 0
        return_param=''
        return_array={}
        for device in glob.glob(base_dir + '28*'):
                count=count+1
                global device_folder
                global device_file
                device_folder = device
                device_file = device_folder + '/w1_slave'
                return_array[sens_name()] = str(read_temp())
        return_param=json.dumps(return_array)
        return return_param

def read_temp_raw():
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    fd = os.open(device_file,os.O_RDONLY)
    lines = os.read(fd,80)
    os.close(fd)
    print lines
#    if os.path.isfile(device_file):
#        f = open(device_file, 'r')
#        lines = f.readlines()
#        f.close()
    return lines

def sens_name():
    sensor = device_folder[-15:]
    return sensor

def read_temp():
    print "reading Temp"
    str = read_temp_raw()
    equals_pos = str.find('t=')
    if equals_pos != -1:
	temp_string = str[equals_pos+2:]
        print temp_string
	if float(temp_string) == float(85000):
		print "ERROR - BAD TEMP"
		temp_c = read_temp()
	else:
		temp_c = float(temp_string) / 1000.0
        return temp_c


# Build data payload
data_in = read_temps_multi()

# data_payload = 'data_in=' + json.dumps(config_io)
data_payload = 'data_in=' + data_in
print data_payload

