#!/usr/bin/python
#report_temp.py

#Exosite Cloud-enabled DS18b20 Temperature sensors

#Imports
import time
from decimal import *
import urllib
import httplib
import os
import glob
import os.path
import sys

class Logger(object):
    def __init__(self, filename="Default.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

sys.stdout = Logger("report_temp.log")

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

#w1 DS18b20 Configuration Setting
base_dir = '/sys/bus/w1/devices/'
device_folder = ''
device_file = ''

#Exosite portal configuration
cik = 'dbf551a3b9332f1888e7c706bbde9689f33e6c38'
server = 'm2.exosite.com'

def read_temps_multi():
        count = 0
        return_param=''
        return_array={}
        for device in glob.glob(base_dir + '28*'):
                count=count+1
                global device_folder
                global device_file
                device_folder = device
                device_file = device_folder + '/w1_slave'
                return_array[sens_name()] = str(read_temp())
        return_param=urllib.urlencode(return_array)
        return return_param

def read_temp_raw():
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    fd = os.open(device_file,os.O_RDONLY)
    lines = os.read(fd,80)
    os.close(fd)
#    print lines
#    if os.path.isfile(device_file):
#        f = open(device_file, 'r')
#        lines = f.readlines()
#        f.close()
    return lines

def sens_name():
    sensor = device_folder[-15:]
    return sensor

def read_temp():
    print "reading Temp"
    str = read_temp_raw()
    equals_pos = str.find('t=')
    if equals_pos != -1:
	temp_string = str[equals_pos+2:]
        print temp_string
	if float(temp_string) == float(85000):
		print "ERROR - BAD TEMP"
		temp_c = read_temp()
	else:
		temp_c = float(temp_string) / 1000.0
        return temp_c


#def read_temp():
#    print "reading Temp"
#    lines = read_temp_raw()
#    while lines[0].strip()[-3:] != 'YES':
#        time.sleep(0.2)
#        lines = read_temp_raw()
#    equals_pos = lines[1].find('t=')
#    if equals_pos != -1:
#        temp_string = lines[1][equals_pos+2:]
#        temp_c = float(temp_string) / 1000.0
#        return temp_c

#while True:
for x in range(0, 1):
# Request data and read the answer
        params = read_temps_multi()# Get Sensor Data
#       print 'Exosite Params to Submit'
#       print params
#       print '========================================================================'
#       print 'Exosite API Using CIK ', cik
#       print '========================================================================'
#       print '\r\n'
#
        url = '/api:v1/stack/alias'
        headers = {'X-Exosite-CIK': cik, 'content-type': 'application/x-www-form-urlencoded; charset=utf-8'}
#       print '=================='
#       print 'POST'
#       print '=================='
#       print 'URL: ', url
#       print 'Data: ', params
#       print ' '
        conn = httplib.HTTPConnection(server)
        conn.set_debuglevel(1)
        conn.request("POST",url,params,headers)
        response = conn.getresponse();
#       print 'response: ',response.status,response.reason
        data = response.read()
        end = data.find('<')
        if -1 == end: end = len(data)

#       print '\r\n\r\n'
        url = '/api:v1/stack/alias?TEMP1&condition'
        headers = {'Accept':'application/x-www-form-urlencoded; charset=utf-8','X-Exosite-CIK':cik}
#       print '=================='
#       print 'GET'
#       print '=================='
#       print 'URL: ', url
#       print ' '
        conn = httplib.HTTPConnection(server)
        conn.request("GET",url,"",headers)
        response = conn.getresponse();
#       print 'response: ',response.status,response.reason
        data = response.read()
        conn.close()
#       print 'response data:', data
#       time.sleep(60)
        break
