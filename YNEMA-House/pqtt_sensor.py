import os
import time
import sys
import smbus
import paho.mqtt.client as mqtt
import json

MQTT_HOST = '10.10.10.204'

# Data capture and upload interval in seconds. Less interval will eventually hang the DHT22.
INTERVAL=2

sensor_data = {'temperature': 0, 'humidity': 0}

next_reading = time.time() 

client = mqtt.Client()

# Set access token
client.username_pw_set(username="mqttuser",password="mqttpassword")

# Connect to MQTT_HOST using default MQTT port and 60 seconds keepalive interval
client.connect(MQTT_HOST, 1883, 60)
client.publish("state_topic": "homeassistant/sensor/raspi02sensor/state", "value_template": "{{ value_json.temperature}}", "name": "temp sensor")

client.loop_start()

# Get I2C bus
bus = smbus.SMBus(1)

try:
    while True:
		# SHT25 address, 0x40(64)
		# Send temperature measurement command
		#               0xF3(243)       NO HOLD master
		bus.write_byte(0x40, 0xF3)

		time.sleep(INTERVAL)

		# SHT25 address, 0x40(64)
		# Read data back, 2 bytes
		# Temp MSB, Temp LSB
		data0 = bus.read_byte(0x40)
		data1 = bus.read_byte(0x40)

		# Convert the data
		temp = data0 * 256 + data1
		temperature = -46.85 + ((temp * 175.72) / 65536.0)

		# SHT25 address, 0x40(64)
		# Send humidity measurement command
		#               0xF5(245)       NO HOLD master
		bus.write_byte(0x40, 0xF5)

		time.sleep(0.5)

		# SHT25 address, 0x40(64)
		# Read data back, 2 bytes
		# Humidity MSB, Humidity LSB
		data0 = bus.read_byte(0x40)
		data1 = bus.read_byte(0x40)

		# Convert the data
		humidity = data0 * 256 + data1
		humidity = -6 + ((humidity * 125.0) / 65536.0)
        
                print(u"Temperature: {:g}\u00b0C, Humidity: {:g}%".format(temperature, humidity))
                sensor_data['temperature'] = temperature
                sensor_data['humidity'] = humidity

                # Sending humidity and temperature data to MQTT_HOST
                client.publish('homeassistant/sensor/raspi02sensor/state', json.dumps(sensor_data), 1)

                next_reading += INTERVAL
                sleep_time = next_reading-time.time()
                if sleep_time > 0:
                    time.sleep(sleep_time)
except KeyboardInterrupt:
    pass

client.loop_stop()
client.disconnect()
