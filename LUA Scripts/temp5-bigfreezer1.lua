-- 
-- This script will check every new temp value against temp&time conditions and notify if required.
-- 
-- Create by Alex Ynema alex@ynema.net
-- Initial Creation: 02/2016 for Boyup Brook IGA
--

--------------- Configure These Variables ----------------------------------------
local probe_ID = '28-0000052f8220'				-- Temperature Probe ID
local notify_freq = 3600						-- Frequency in Seconds between notifications
local warn_period = 3600 						-- Warning Time average in seconds
local crit_period = 5400 						-- Critical Time average in seconds
local warn_temp = 0		 						-- Warning Trigger Temperature in Degrees Celsius
local crit_temp = -3							-- Critical Trigger Temperature in Degrees Celsius
local send_email = false						-- Send email notifications
local send_sms = false							-- Send SMS Alert Notifications
local Run_Script = false
--------------- Important System Variables ---------------------------------------
local contact_email = alias['contact_email'] 	-- Email Address Datasource
local contact_sms = alias['contact_sms']		-- sms Number Datasource
local user_number = ''
local email_recipient = ''
local dataport_trigger = alias[probe_ID] 					-- Dataport to trigger script
local dataport_alias = alias[probe_ID]   					-- Dataport to monitor
local dataport_warn_alarm_notify = alias["warn-alarm-"..probe_ID.."-notify"]	-- Last notification time
local dataport_crit_alarm_notify = alias["crit-alarm-"..probe_ID.."-notify"]	-- Last notification time
local probe_name = dataport_trigger.name					-- Human Readable Dataport Name
local myaverage = alias['averagealias']                 			-- Data source to put average value into
---------------- Enable/Disable Debug Settings -----------------------------------
local test_functions = false 							-- true/false to enable
----------------------------------------------------------------------------------


-- Functions
-- Func: prettydebug
-- Desc: Call debug with specific format and text
local function prettydebug(text,name)
	if test_functions then
		if name == nil then name = "main" end
		local debugtext = string.format(" [%s] %s",name,text)
		debug(debugtext)
	end
end
-- Func: round
-- Desc: return rounded number to a number of decimal places
local function round(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

-- Func: rounds
-- Desc: return rounded number as string to a number of decimal places
local function rounds(num,idp)
  return string.format("%." .. (idp or 0) .. "f", num)
end

-- Func: get_average
-- Desc: return calculated average for data source 
--       over the time period given
local function get_average(alias,p_start,p_end)
	local func_name = 'func_avg'
	local done = false
	local datapts = 0
	local average = 0

	dataport_alias.last = p_start

	while done == false do
		local ts = dataport_alias.wait(p_end)
		if ts ~= nil then
			local value = dataport_alias[ts] -- value at timestamp
			prettydebug(string.format("Found Data Point: timestamp %d, value of %s",ts,rounds(value,2)),func_name)
			average = average + value
			datapts = datapts + 1
		else
			prettydebug(string.format("Found no more values, done with routine"),func_name)
			done = true
		end
	end
	prettydebug(string.format("Found %d points, calculating average...",datapts),func_name)
	if datapts > 0 then
		average = average/datapts
		prettydebug(string.format("Average: %s",rounds(average,2)),func_name)
	return average,datapts
	else
		prettydebug(string.format("No points found, average not calculated"),func_name)
		return nil,0
	end
end
local function createDataport(alias,dataport_type)
	prettydebug("Creating dataport for: "..alias..".")
	local status, rid = manage.create(
		"dataport",
		{
		format = dataport_type,
		name = alias,
		retention = {count = "infinity" ,duration = "infinity"}
		}
	)
	if status == false then
		prettydebug("Error Creating Dataport: "..rid.." Value: "..tostring(status))
	else
		local status, ret = manage.map("alias", rid, alias)    
	        if status == false then
			prettydebug("Error Mapping Alias to Dataport: "..ret)
 		end
	end
	return status
end

local function check_dataport(dataport_alias,dataport_type)
  if dataport_alias ~= nil then
	local status, rid = manage.lookup("aliased",dataport_alias)
	prettydebug("Checking if '"..dataport_alias.."' exists")
	if dataport_type == nil then dataport_type = "integer" end
	if status == false then
		prettydebug("Creating Dataport:"..dataport_alias.." of type: "..dataport_type)
		createDataport(dataport_alias,dataport_type)
	else
		prettydebug("Dataport Exists")
	end
  else
	  prettydebug("No Dataport Alias Sent")
  end
end

local function send_notification(warn_type,current_temp,warn_since,period)
  local func_name = 'send_notification'
  current_temp = round(current_temp,2)
  local email_sub = "" 
  local sms_msg = ""
  local email_msg = "" 
  if warn_type =="Recovered" then
	sms_msg = sms_msg..probe_name.." has recovered and is at: "..current_temp.."C"
	email_sub = email_sub..probe_name.." has recovered and is at: "..current_temp.."°C"
  	email_msg = email_msg..probe_name.." has recovered\r\n\r\n"
  	email_msg = email_msg..probe_name.." is averaging "..current_temp.."°C for the past "..(period/60).." minutes\r\n\r\n"
  	email_msg = email_msg.."Notify Frequency: "..(notify_freq/60).." minutes\r\n"
  	email_msg = email_msg.."Warn Alarm is above: "..warn_temp.."°C for "..(warn_period/60).." minutes\r\n"
  	email_msg = email_msg.."Crit Alarm is above: "..crit_temp.."°C for "..(crit_period/60).." minutes\r\n"
  	email_msg = email_msg.."\r\n\r\nMore Details: https://portals.exosite.com/views/2636948555/1812337420"
  else
  	sms_msg = sms_msg..warn_type.." Alarm!! "..probe_name.." is about "..current_temp.."C Alarm active for "..round(((now-warn_since)/60),0).." min"
  	email_sub = email_sub..warn_type..": "..probe_name.." is at temperature: "..current_temp.."°C"
  	email_msg = email_msg..warn_type.." Alarm!!\r\n\r\n"
  	email_msg = email_msg..probe_name.." Temperature Probe Report\r\n\r\n"
  	email_msg = email_msg.."The alarm started at "..date('%c',warn_since).." and has been active for "..round(((now-warn_since)/60),0).." minutes\r\n"
  	email_msg = email_msg..probe_name.." is averaging "..current_temp.."°C for the past "..(period/60).." minutes\r\n\r\n"
  	email_msg = email_msg.."Notify Frequency: "..(notify_freq/60).." minutes\r\n"
  	email_msg = email_msg.."Warn Alarm is above: "..warn_temp.."°C for "..(warn_period/60).." minutes\r\n"
  	email_msg = email_msg.."Crit Alarm is above: "..crit_temp.."°C for "..(crit_period/60).." minutes\r\n"
  	email_msg = email_msg.."\r\n\r\nMore Details: https://portals.exosite.com/views/2636948555/1812337420"
  end
  if test_functions then
  	prettydebug("Sending Email Notification to: "..email_recipient.." and SMS to: "..user_number,func_name)
  	prettydebug("Warn Type: "..warn_type.." Current Temp: "..current_temp.." Alarm Start: "..date('%c',warn_since),func_name)
	prettydebug(email_msg,func_name)
	prettydebug(sms_msg,func_name)
	email_msg = email_msg.."DEBUGGING ON"
	sms_msg = sms_msg.."DEBUG ON"
  end
  if send_email then
	dispatch.email(email_recipient, email_sub, email_msg)
  end
  if send_sms then
  	dispatch.sms(user_number, sms_msg)
  end
end


--
-- Main Application Code --
--

debug("[Main] --")
debug("[Main] -------------------------")
debug("[Main] Start Monitoring Program")
if test_functions then
  debug("[Main] Debugging: ON")
  email_recipient = 'alex@ynema.net'		
  user_number = '+61404796894'
  prettydebug("Setting Debug Notifications to: "..email_recipient.." and SMS to: "..user_number)
  check_dataport("contact_email","string")
  check_dataport("contact_sms","string")
  if contact_email.value == nil or contact_sms.value == nil then
	prettydebug("No Contact Variables Set")
  else
	prettydebug("Contact Variables Set")
  	prettydebug("Contact email: "..contact_email.value.." and SMS: "..contact_sms.value)
  end
else
  debug("[Main] Debugging: OFF")
  if contact_email.value ~= nil and contact_sms.value ~= nil then
  	email_recipient = contact_email.value 	-- Notification email address
  	user_number = contact_sms.value		-- SMS Number be sure to use correct country code
  else
	debug("ERROR - No Notification Variables")
  end
end

debug("[Main] -------------------------")
settimezone("Australia/Perth") -- use UTC as timezone

-- Check if Dataports for the Probe exist or create them if not
check_dataport(probe_ID)
check_dataport(probe_ID.."_10min_avg")
check_dataport(probe_ID.."_30min_avg")
check_dataport(probe_ID.."_60min_avg")
check_dataport("warn-alarm-"..probe_ID.."-status","string")
check_dataport("warn-alarm-"..probe_ID.."-notify")
check_dataport("crit-alarm-"..probe_ID.."-status","string")
check_dataport("crit-alarm-"..probe_ID.."-notify")



while Run_Script do
  prettydebug("----------------------")
  prettydebug("Start of Process Loop")
  prettydebug("----------------------")

  local timestamp = dataport_trigger.wait()
  local ts_period_end = now
  local current_temp = round(dataport_alias.value,2)
  local warn_avg,pts = get_average(mydataport,(ts_period_end-warn_period),ts_period_end)
  local crit_avg,pts = get_average(mydataport,(ts_period_end-crit_period),ts_period_end)
  local dataport_warn_alarm_on = alias["warn-alarm-"..probe_ID.."-status"] 	-- Current Warning Alarm Status
  local dataport_crit_alarm_on = alias["crit-alarm-"..probe_ID.."-status"] 	-- Current Critical Alarm Status

  local dataport_10min_avg = alias[probe_ID.."_10min_avg"] 	-- Current Critical Alarm Status
  local current_avg,pts = get_average(mydataport,(ts_period_end-600),ts_period_end)
  dataport_10min_avg.value = current_avg
  prettydebug("Current 10min Avg: "..current_avg.."°C")

  local dataport_30min_avg = alias[probe_ID.."_30min_avg"] 	-- Current Critical Alarm Status
  local current_avg,pts = get_average(mydataport,(ts_period_end-1800),ts_period_end)
  dataport_30min_avg.value = current_avg
  prettydebug("Current 30min Avg: "..current_avg.."°C")
  
  local dataport_60min_avg = alias[probe_ID.."_60min_avg"] 	-- Current Critical Alarm Status
  local current_avg,pts = get_average(mydataport,(ts_period_end-3600),ts_period_end)
  dataport_60min_avg.value = current_avg
  prettydebug("Current 60min Avg: "..current_avg.."°C")

  if dataport_warn_alarm_on.value == nil or dataport_crit_alarm_on.value == nil then
	prettydebug("Either Alarm is currently un-configured Setting to False")
	dataport_crit_alarm_on.value = false
	dataport_warn_alarm_on.value = false
  end

  if warn_avg ~= nil and crit_avg ~= nil then
	myaverage[warn_avg_temp] = round(warn_avg,2)
	myaverage[crit_avg_temp] = round(crit_avg,2)
	prettydebug("Warning  avg: "..warn_avg.."°C Critical Avg: "..crit_avg.."°C")
	prettydebug("Warning temp: "..warn_temp.."°C Critical temp: "..crit_temp.."°C")
	prettydebug("Warning time: "..warn_period.."s Critical time: "..crit_period.."s")
	if crit_avg>=crit_temp then
		prettydebug("Critical Warning Triggered")
		if dataport_crit_alarm_on.value=="false" then 
			prettydebug("Setting Warn to True")
			dataport_crit_alarm_on.value = true
		end
		if dataport_warn_alarm_on.value=="false" then 
			prettydebug("Setting Warn to True")
			dataport_warn_alarm_on.value = true 
		end
		if  dataport_crit_alarm_notify.value==nil or dataport_crit_alarm_notify.value<=(timestamp-notify_freq) then
			prettydebug("Sending Notification")
			send_notification("Critical",crit_avg,dataport_crit_alarm_on.timestamp,crit_period)
			dataport_crit_alarm_notify.value = timestamp
		else
			prettydebug("Not Sending Notification")
		end
	end
	if warn_avg>=warn_temp and crit_avg<crit_temp then
		prettydebug("Non-Critical Warning Triggered")
		if dataport_warn_alarm_on.value=="false" then 
			prettydebug("Setting Warn to True")
			dataport_warn_alarm_on.value = true 
		end
		if dataport_crit_alarm_on.value=="true" then 
			prettydebug("Setting Crit to False")
			dataport_crit_alarm_on.value = false 
		end
		if dataport_warn_alarm_notify.value==nil or dataport_warn_alarm_notify.value<=(timestamp-notify_freq) then
			prettydebug("Sending Notification")
			send_notification("Warning",warn_avg,dataport_warn_alarm_on.timestamp,warn_period)
			dataport_warn_alarm_notify.value = timestamp
		else
			prettydebug("Not Sending Notification")
		end
	end
	if warn_avg<warn_temp and crit_avg<crit_temp then
		prettydebug("Temperature Currently Fine")
		prettydebug("Warn Trigger: "..dataport_warn_alarm_on.value)
		prettydebug("Crit Trigger: "..dataport_crit_alarm_on.value)
		if dataport_crit_alarm_on.value=="true" then 
			prettydebug("Setting Crit to False")
			dataport_crit_alarm_on.value = false 
			dataport_warn_alarm_on.value = false 
			send_notification("Recovered",crit_avg,dataport_warn_alarm_on.timestamp,crit_period)
		end
		if dataport_warn_alarm_on.value=="true" then 
			prettydebug("Setting Warn to False")
			dataport_warn_alarm_on.value = false 
			send_notification("Recovered",warn_avg,dataport_warn_alarm_on.timestamp,warn_period)
		end
	end
        prettydebug("Warn Trigger: "..tostring(dataport_warn_alarm_on.value).." Triggered at "..date('%c',dataport_warn_alarm_on.timestamp))
        prettydebug("Crit Trigger: "..tostring(dataport_crit_alarm_on.value).." Triggered at "..date('%c',dataport_crit_alarm_on.timestamp))
  else
	prettydebug("Error warn_avg or crit_avg not valid")
  end
  prettydebug("End of Process Loop")
  prettydebug("----------------------")
end
prettydebug("-- End of Program --")
prettydebug("--------------------")
prettydebug("---- Created By ----")
prettydebug("---- Alex Ynema ----")
prettydebug("--------------------")
prettydebug("    ")