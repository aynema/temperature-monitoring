-- 
-- This script generate a report and email it for the Probe_ID
-- 
-- Create by Alex Ynema alex@ynema.net
-- Initial Creation: 02/2016 for Boyup Brook IGA
--

--------------- Configure These Variables ----------------------------------------
local probe_ID = '28-80000026db55'						-- Temperature Probe ID
local notify_freq = 3600							-- Frequency in Seconds between notifications
local warn_period = 0000 							-- Warning Time average in seconds
local crit_period = 0000 							-- Critical Time average in seconds
local warn_temp = 0000		 						-- Warning Trigger Temperature in Degrees Celsius
local crit_temp = 0000								-- Critical Trigger Temperature in Degrees Celsius
local send_email = true								-- Send email notifications
local send_sms = true								-- Send SMS Alert Notifications
--------------- Important System Variables ---------------------------------------
local contact_email = alias['contact_email'] 					-- Email Address Datasource
local contact_sms = alias['contact_sms']					-- sms Number Datasource
local user_number = ''
local email_recipient = ''
local dataport_trigger = alias[probe_ID] 					-- Dataport to trigger script
local dataport_alias = alias[probe_ID]   					-- Dataport to monitor
local dataport_warn_alarm_notify = alias["warn-alarm-"..probe_ID.."-notify"]	-- Last notification time
local dataport_crit_alarm_notify = alias["crit-alarm-"..probe_ID.."-notify"]	-- Last notification time
local probe_name = dataport_trigger.name					-- Human Readable Dataport Name
local myaverage = alias['averagealias']                 			-- Data source to put average value into
---------------- Enable/Disable Debug Settings -----------------------------------
local test_functions = false							-- true/false to enable
----------------------------------------------------------------------------------


-- Functions
-- Func: prettydebug
-- Desc: Call debug with specific format and text
local function prettydebug(text,name)
	if test_functions then
		if name == nil then name = "main" end
		local debugtext = string.format(" [%s] %s",name,text)
		debug(debugtext)
	end
end
-- Func: round
-- Desc: return rounded number to a number of decimal places
local function round(num, idp)
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

-- Func: rounds
-- Desc: return rounded number as string to a number of decimal places
local function rounds(num,idp)
  return string.format("%." .. (idp or 0) .. "f", num)
end

-- Func: get_average
-- Desc: return calculated average for data source 
--       over the time period given
local function get_average(alias,p_start,p_end)
	local func_name = 'func_avg'
	local done = false
	local datapts = 0
	local average = 0

	dataport_alias.last = p_start

	while done == false do
		local ts = dataport_alias.wait(p_end)
		if ts ~= nil then
			local value = dataport_alias[ts] -- value at timestamp
			prettydebug(string.format("Found Data Point: timestamp %d, value of %s",ts,rounds(value,2)),func_name)
			average = average + value
			datapts = datapts + 1
		else
			prettydebug(string.format("Found no more values, done with routine"),func_name)
			done = true
		end
	end
	prettydebug(string.format("Found %d points, calculating average...",datapts),func_name)
	if datapts > 0 then
		average = average/datapts
		prettydebug(string.format("Average: %s",rounds(average,2)),func_name)
	return average,datapts
	else
		prettydebug(string.format("No points found, average not calculated"),func_name)
		return nil,0
	end
end
local function createDataport(alias,dataport_type)
	prettydebug("Creating dataport for: "..alias..".")
	local status, rid = manage.create(
		"dataport",
		{
		format = dataport_type,
		name = alias,
		retention = {count = "infinity" ,duration = "infinity"}
		}
	)
	if status == false then
		prettydebug("Error Creating Dataport: "..rid.." Value: "..tostring(status))
	else
		local status, ret = manage.map("alias", rid, alias)    
	        if status == false then
			prettydebug("Error Mapping Alias to Dataport: "..ret)
 		end
	end
	return status
end

local function check_dataport(dataport_alias,dataport_type)
  if dataport_alias ~= nil then
	local status, rid = manage.lookup("aliased",dataport_alias)
	prettydebug("Checking if '"..dataport_alias.."' exists")
	if dataport_type == nil then dataport_type = "integer" end
	if status == false then
		prettydebug("Creating Dataport:"..dataport_alias.." of type: "..dataport_type)
		createDataport(dataport_alias,dataport_type)
	else
		prettydebug("Dataport Exists")
	end
  else
	  prettydebug("No Dataport Alias Sent")
  end
end

local function send_notification(warn_type,current_temp,warn_since,period)
  local func_name = 'send_notification'
  current_temp = round(current_temp,2)
  local email_sub = "" 
  local sms_msg = ""
  local email_msg = "" 
  if warn_type =="Recovered" then
	sms_msg = sms_msg..probe_name.." has recovered and is at: "..current_temp.."C"
	email_sub = email_sub..probe_name.." has recovered and is at: "..current_temp.."°C"
  	email_msg = email_msg..probe_name.." has recovered\r\n\r\n"
  	email_msg = email_msg..probe_name.." is averaging "..current_temp.."°C for the past "..(period/60).." minutes\r\n\r\n"
  	email_msg = email_msg.."Notify Frequency: "..(notify_freq/60).." minutes\r\n"
  	email_msg = email_msg.."Warn Alarm is above: "..warn_temp.."°C for "..(warn_period/60).." minutes\r\n"
  	email_msg = email_msg.."Crit Alarm is above: "..crit_temp.."°C for "..(crit_period/60).." minutes\r\n"
  	email_msg = email_msg.."\r\n\r\nMore Details: https://portals.exosite.com/views/2636948555/1812337420"
  else
  	sms_msg = sms_msg..warn_type.." Alarm!! "..probe_name.." is about "..current_temp.."C Alarm active for "..round(((now-warn_since)/60),0).." min"
  	email_sub = email_sub..warn_type..": "..probe_name.." is at temperature: "..current_temp.."°C"
  	email_msg = email_msg..warn_type.." Alarm!!\r\n\r\n"
  	email_msg = email_msg..probe_name.." Temperature Probe Report\r\n\r\n"
  	email_msg = email_msg.."The alarm started at "..date('%c',warn_since).." and has been active for "..round(((now-warn_since)/60),0).." minutes\r\n"
  	email_msg = email_msg..probe_name.." is averaging "..current_temp.."°C for the past "..(period/60).." minutes\r\n\r\n"
  	email_msg = email_msg.."Notify Frequency: "..(notify_freq/60).." minutes\r\n"
  	email_msg = email_msg.."Warn Alarm is above: "..warn_temp.."°C for "..(warn_period/60).." minutes\r\n"
  	email_msg = email_msg.."Crit Alarm is above: "..crit_temp.."°C for "..(crit_period/60).." minutes\r\n"
  	email_msg = email_msg.."\r\n\r\nMore Details: https://portals.exosite.com/views/2636948555/1812337420"
  end
  if send_email then
	dispatch.email(email_recipient, email_sub, email_msg)
  end
  if send_sms then
  	dispatch.sms(user_number, sms_msg)
  end
  if test_functions then
  	prettydebug("Sending Email Notification to: "..email_recipient.." and SMS to: "..user_number,func_name)
  	prettydebug("Warn Type: "..warn_type.." Current Temp: "..current_temp.." Alarm Start: "..date('%c',warn_since),func_name)
	prettydebug(email_msg,func_name)
	prettydebug(sms_msg,func_name)
  end
end


--
-- Main Application Code --
--

debug("[Main] --")
debug("[Main] -------------------------")
debug("[Main] Start Monitoring Program")
if test_functions then
  debug("[Main] Debugging: ON")
  email_recipient = 'alex@ynema.net'		
  user_number = '+61404796894'
  prettydebug("Setting Debug Notifications to: "..email_recipient.." and SMS to: "..user_number)
  check_dataport("contact_email","string")
  check_dataport("contact_sms","string")
  if contact_email.value == nil or contact_sms.value == nil then
	prettydebug("No Contact Variables Set")
  else
	prettydebug("Contact Variables Set")
  	prettydebug("Contact email: "..contact_email.value.." and SMS: "..contact_sms.value)
  end
else
  debug("[Main] Debugging: OFF")
  if contact_email.value ~= nil and contact_sms.value ~= nil then
  	email_recipient = contact_email.value 	-- Notification email address
  	user_number = contact_sms.value		-- SMS Number be sure to use correct country code
  else
	debug("ERROR - No Notification Variables")
  end
end

debug("[Main] -------------------------")
settimezone("Australia/Perth") -- use UTC as timezone

-- Check if Dataports for the Probe exist or create them if not
prettydebug("-- Check probe_ID Exists")
check_dataport(probe_ID)

prettydebug("-- Set ts_period_end")
local ts_period_end = now
--prettydebug("-- Round Current Temp")
local current_temp = round(dataport_alias.value,2)
prettydebug("-- Get 60 Average")
local current_avg,pts = get_average(mydataport,(ts_period_end-600),ts_period_end)

prettydebug("-- Start Email Creation")
local email_sub = "Temperature report for "..probe_name..""
local email_msg = "Temperature report for "..probe_name.."\r\n\r\n"
email_msg = email_msg..probe_name.." current temp is: "..current_temp.."°C\r\n\r\n"

email_msg = email_msg.."Temp Average for past 10m is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-1200),ts_period_end)
email_msg = email_msg.."Temp Average for past 20m is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-1800),ts_period_end)
email_msg = email_msg.."Temp Average for past 30m is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-2400),ts_period_end)
email_msg = email_msg.."Temp Average for past 40m is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-3000),ts_period_end)
email_msg = email_msg.."Temp Average for past 50m is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-3600),ts_period_end)
email_msg = email_msg.."Temp Average for past 1Hr is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-7200),ts_period_end)
email_msg = email_msg.."Temp Average for past 2Hr is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-10800),ts_period_end)
email_msg = email_msg.."Temp Average for past 3Hr is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-14400),ts_period_end)
email_msg = email_msg.."Temp Average for past 4Hr is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-18000),ts_period_end)
email_msg = email_msg.."Temp Average for past 5Hr is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-21600),ts_period_end)
email_msg = email_msg.."Temp Average for past 6Hr is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-43200),ts_period_end)
email_msg = email_msg.."Temp Average for past 12Hr is :"..current_avg.."°C\r\n"
current_avg,pts = get_average(mydataport,(ts_period_end-86400),ts_period_end)
email_msg = email_msg.."Temp Average for past 24Hr is :"..current_avg.."°C\r\n\r\n"
email_msg = email_msg.."\r\n\r\nMore Details: https://portals.exosite.com/views/2636948555/1812337420"

--debug(email_msg)
dispatch.email(email_recipient, email_sub, email_msg)

prettydebug("-- End of Program --")
prettydebug("--------------------")
prettydebug("---- Created By ----")
prettydebug("---- Alex Ynema ----")
prettydebug("--------------------")
prettydebug("    ")