* Software for a RasberryPI based Server Room Monitoring system
	* Designed and built by Alex Ynema | Open Systems Support | alex@opensystems.net.au
* System Configuration & Installation Details
	* Circuit Diagram: http:// <TBA>
	* Hardware: RasberryPi with Custom Shield to connect DS18B20 Temperature Sensors &
	* OS: Pidora-2014-R3 (Should work on others)
	* Additional Software Installation requried
		* Pidora
			* yum install screen wget vim net-snmp net-snmp-utils python-rpi-gpio
		* Rasbian
			* apt-get install screen wget vim snmp python-rpi.gpio
	* Custom Shield Assumptions (ie What connects to which RasPi GPIO Pins)
		* GPIO #04: DS18B20 Data Pickup
		* GPIO #18: Leak Detector #1
		* GPIO #23: Leak Detector #2
		* GPIO #24: LED1 
		* GPIO #25: LED2
* Software Assumptions
	* Assuming downloading GIT Repo into /root/
	* Exosite Account configured with correct variables ie
	* Device: 		MAC ADDRESS of RasberryPi (Can be what ever though)
	* Data:		Each probe DS18B20 ID connected (ie 28-0000024d8b2f)
		* If they are connected run the following
		* modprobe w1-gpio && modprobe w1-therm
		* The probe ID's should be listed in /sys/bus/w1/devices/
			* **NOTE**
			* Newest RASBIAN with Pi2 support breaks things due to a new 'Device Tree' setup so you will need to do these additional steps to get it working.
				* Edit /boot/config.txt
				* Add the line dtoverlay=w1-gpio
				* Reboot the Pi
* Setup Steps
	1.  Download Git Repo
		1. git clone https://aynema@bitbucket.org/aynema/temperature-monitoring.git
	2. Modify Exosite account details ie
		* vi /root/temperature-monitoring/BBIGA-TEMP01/report_temperature.py
		* enter Exosite Module CIKS (This is the Exosite Device ID) ie
			* cik = '627c69c0e77c24c19ad24474d022f44c1bd9705d'
	3. Setup Crontab job ie.
		* \*/2 \* \* \* \* /bin/python /root/temperature-monitoring/BBIGA-TEMP01/report_temperature.py
	4. SNMP Setup (Optional)
		1. cp /usr/local/bin/temperature-monitoring/snmp/snmpd.conf /etc/snmp/snmpd.conf
		2. Modify the following files
			- /etc/snmp/snmpd.conf
				+ rocommunity public 172.17.3.0/24
				+ pass  .1.3.6.1.2.1.25.1.11      /bin/sh         /usr/local/bin/temperature-monitoring/snmp/snmp-temp 1
				+ /usr/local/bin/temperature-monitoring/snmp/snmp-temp
				+ \- 1)
					+ echo .1.3.6.1.2.1.25.1.11
					+ echo gauge
					+ python /usr/local/bin/temperature-monitoring/snmp/temp.py 28-00000606348d
					+ ;;
		3. Testing & Final Checks
			1. Check Service starts
				- chkconfig snmpd on
				- chkconfig iptables on 
			2. Test the SNMP Probes
				- snmpget -v 1 -c public 172.17.3.230 .1.3.6.1.2.1.25.1.11
	5. Leak Detection Probes (Optional)
		1. <TBA>